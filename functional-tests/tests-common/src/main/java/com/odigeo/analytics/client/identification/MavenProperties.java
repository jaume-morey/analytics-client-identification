package com.odigeo.analytics.client.identification;

public class MavenProperties {
    public static final String TEST_SEED = System.getProperty("odigeo.test.seed");
    public static final String APPLICATION_HOST = System.getProperty("functionaltests.targetserver");
}

package com.odigeo.analytics.client.identification.v1;

import com.odigeo.analytics.client.identification.api.v1.AnalyticsClientIdentificationService;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;

public class AnalyticsClientIdentificationController implements AnalyticsClientIdentificationService {

    private static final Logger logger = Logger.getLogger(AnalyticsClientIdentificationController.class);

    @Override
    public Response identifyClient(String clientId) {
        logger.info("call received with clientId " + clientId);
        return Response.ok().build();
    }
}

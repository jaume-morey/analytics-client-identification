package com.odigeo.analytics.client.identification.rest;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.analytics.client.identification.v1.AnalyticsClientIdentificationController;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

public class ServiceApplication extends Application {

    private final Set<Object> singletons = new HashSet<>();

    public ServiceApplication() {
        ConfigurationEngine.init();
        singletons.add(ConfigurationEngine.getInstance(AnalyticsClientIdentificationController.class));
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}

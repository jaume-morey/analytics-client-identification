package com.odigeo.analytics.client.identification.monitoring;

import com.codahale.metrics.health.HealthCheck;

public class BaseHealthCheck extends HealthCheck {

    @Override
    protected HealthCheck.Result check() {
        return Result.healthy("analytics-client-identification is running correctly");
    }
}

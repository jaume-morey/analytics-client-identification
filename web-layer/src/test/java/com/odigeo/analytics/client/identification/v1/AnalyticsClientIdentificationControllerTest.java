package com.odigeo.analytics.client.identification.v1;

import com.odigeo.commons.monitoring.metrics.MetricsManager;
import com.odigeo.commons.monitoring.metrics.reporter.ReporterStatus;
import org.apache.log4j.Appender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.WriterAppender;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;
import java.io.StringWriter;

import static org.testng.Assert.assertEquals;

public class AnalyticsClientIdentificationControllerTest {

    private AnalyticsClientIdentificationController controller = new AnalyticsClientIdentificationController();

    @BeforeClass
    public void setUpClass() {
        MetricsManager.getInstance().changeReporterStatus(ReporterStatus.STOPPED);

        StringWriter log = new StringWriter();
        Appender appender = new WriterAppender(new PatternLayout("%p, %m%n"), log);
        Logger.getLogger(AnalyticsClientIdentificationController.class).addAppender(appender);
    }

    @Test
    public void testIdentifyClient() {
        assertEquals(controller.identifyClient("12abc34def").getStatus(), Response.Status.OK.getStatusCode());
    }

}

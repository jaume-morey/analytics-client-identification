package com.odigeo.analytics.client.identification.rest;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ServiceApplicationTest {

    private ServiceApplication serviceApplication = new ServiceApplication();

    @Test
    public void testServiceApplicationSingletons() {
        assertEquals(serviceApplication.getSingletons().size(), 1);
    }

}

package com.odigeo.analytics.client.identification.monitoring;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class BaseHealthCheckTest {

    private BaseHealthCheck baseHealthCheck = new BaseHealthCheck();

    @Test
    public void testCheck() {
        assertEquals(baseHealthCheck.execute().getMessage(), "analytics-client-identification is running correctly");
    }
}
